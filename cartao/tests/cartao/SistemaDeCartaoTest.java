package cartao;

import static org.junit.Assert.*;

import org.junit.Test;

public class SistemaDeCartaoTest {

	@Test
	public void CartaoVisa4111111111111111() {	
		String tipoCartao = new SistemaDeCartao().identificar("4111111111111111");
		
		assertEquals("visa", tipoCartao);
	}

}
